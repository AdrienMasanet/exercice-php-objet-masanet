<?php

//-----[CLASSES]-----\\
class Zoo
{
    // Propriétés
    private $name;
    private $enclosures = [];

    // Méthodes
    public function __construct(string $name = "Zoo sans nom")
    {
        $this->setName($name);
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEnclosures()
    {
        return $this->enclosures;
    }

    public function getEnclosure(int $enclosureIndex)
    {
        return $this->enclosures[$enclosureIndex];
    }

    public function addEnclosure(Enclosure $enclosureToPush)
    {
        array_push($this->enclosures, $enclosureToPush);
    }

    public function removeEnclosure(Enclosure $enclosureToRemove)
    {
        foreach ($this->enclosures as $key => $currentEnclosure) {
            if ($currentEnclosure == $enclosureToRemove) {
                unset($this->enclosures[$key]);
            }
        }
    }
}

class Enclosure
{
    // Propriétés
    private $name;
    private $animals = [];
    private $animalLimit;

    // Méthodes
    public function __construct(string $name = "Enclos sans nom", Zoo $parentZoo, int $animalLimit = 10)
    {
        $this->setName($name);
        $this->setAnimalLimit($animalLimit);
        $parentZoo->addEnclosure($this);
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function setAnimalLimit(int $animalLimit)
    {
        $this->animalLimit = $animalLimit;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAnimals()
    {
        return $this->animals;
    }

    public function getAnimal(int $animalIndex)
    {
        return $this->animals[$animalIndex];
    }

    public function addAnimal(Animal $animalToPush)
    {
        if (count($this->animals) < $this->animalLimit) {
            array_push($this->animals, $animalToPush);
            $animalToPush->setAssignedEnclosure($this);
        } else {
            error_log("CANNOT ADD MORE ANIMALS FOR ENCLOSURE \"" . $this->getName() . "\" BECAUSE LIMIT WAS REACHED.");
        }
    }

    public function removeAnimal(Animal $animalToRemove)
    {
        foreach ($this->animals as $key => $currentAnimal) {
            if ($currentAnimal == $animalToRemove) {
                unset($this->animals[$key]);
            }
        }
    }
}

abstract class Animal
{
    // Propriétés
    private $assignedEnclosure;
    private $gender;
    private $name;
    private $age;
    private $family = "indéfini";
    protected $race;

    // Méthodes
    public function __construct(string $name = "Animal sans nom", string $gender, int $age,  Enclosure $parentEnclosure)
    {
        $this->setName($name);
        $this->setAge($age);

        if (is_a($this, "Bird")) {
            $this->family = "Oiseau";
        } else if (is_a($this, "Fish")) {
            $this->family = "Poisson";
        } else if (is_a($this, "Mammal")) {
            $this->family = "Mammifère";
        } else if (is_a($this, "Reptile")) {
            $this->family = "Reptile";
        }

        $this->gender = $gender;
        $parentEnclosure->addAnimal($this);
    }

    public function setAssignedEnclosure(Enclosure $enclosure)
    {
        $this->assignedEnclosure = $enclosure;
        return $this;
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function setAge(int $age)
    {
        $this->age = $age;
        return $this;
    }

    public function getAssignedEnclosure()
    {
        return $this->assignedEnclosure;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function getRace()
    {
        return $this->race;
    }

    public function doSomething(string $thingToDo)
    {
        echo ($this->name . " " . $thingToDo . "<br />");
    }
}

interface Bird
{
    public function fly();
}

interface Fish
{
    public function swim();
}

interface Mammal
{
    public function breastFeed(Mammal $anotherMammal);
}

interface Reptile
{
    public function sunBathe();
}

class Parrot extends Animal implements Bird
{
    public function __construct(string $name = "Animal sans nom", string $gender, int $age,  Enclosure $parentEnclosure)
    {
        parent::__construct($name, $gender, $age, $parentEnclosure);
        $this->race = "Perroquet";
    }

    public function fly()
    {
        echo ("Même si les perroquets n'aiment pas spécialement voler très haut,  " . $this->getName() . " s'envole librement et gracieusement." . "<br />");
    }
}

class Carp extends Animal implements Fish
{
    public function __construct(string $name = "Animal sans nom", string $gender, int $age,  Enclosure $parentEnclosure)
    {
        parent::__construct($name, $gender, $age, $parentEnclosure);
        $this->race = "Carpe";
    }

    public function swim()
    {
        echo ("La carpe " . $this->getName() . " s'amuse à faire des sprints au fond de l'eau." . "<br />");
    }
}

class ClownFish extends Animal implements Fish
{
    public function __construct(string $name = "Animal sans nom", string $gender, int $age,  Enclosure $parentEnclosure)
    {
        parent::__construct($name, $gender, $age, $parentEnclosure);
        $this->race = "Poisson-Clown";
    }

    public function swim()
    {
        echo ("Le Poisson-Clown " . $this->getName() . " nage tranquillement en zigzaguant entre les anémones." . "<br />");
    }
}

class Panda extends Animal implements Mammal
{
    public function __construct(string $name = "Animal sans nom", string $gender, int $age,  Enclosure $parentEnclosure)
    {
        parent::__construct($name, $gender, $age, $parentEnclosure);
        $this->race = "Panda";
    }

    public function breastFeed(Mammal $anotherMammal)
    {
        if ($anotherMammal instanceof Animal) {
            if ($anotherMammal instanceof Mammal) {
                if ($this->getGender() == "female") {
                    echo ($this->getName() . " donne le sein à " . $anotherMammal->getName() . " qui se met à téter goulument pour s'alimenter." . "<br />");
                } else {
                    echo ($this->getName() . " est un Mâle !!! Il ne peut pas allaiter " . $anotherMammal->getName() . "." . "<br />");
                }
            } else {
                echo ($this->getName() . " ne peut pas allaiter " . $anotherMammal->getName() . " car ça n'est pas un mammifère." . "<br />");
            }
        } else {
            echo ($this->getName() . " ne peut pas allaiter ça. Les mammifères ne peuvent allaiter que des êtres vivants, et des mammifères de surcroît." . "<br />");
        }
    }
}

class Snake extends Animal implements Reptile
{
    public function __construct(string $name = "Animal sans nom", string $gender, int $age,  Enclosure $parentEnclosure)
    {
        parent::__construct($name, $gender, $age, $parentEnclosure);
        $this->race = "Serpent";
    }

    public function sunBathe()
    {
        echo ("Les serpents, comme tous les autres reptiles, ont besoin de soleil. " . $this->getName() . " se met donc à prendre un bon bain de soleil sur une roche bien exposée à la lumière." . "<br />");
    }

    public function throwTongue()
    {
        echo ("Comme tout bon serpent qui se respecte, " . $this->getName() . " sort sa langue et l'agite en une fraction de seconde pour mieux se repérer dans son environnement." . "<br />");
    }

    public function crawl()
    {
        echo ($this->getName() . ", en tant que bon serpent se met à ramper lentement mais sûrement. Où va-t-il et pourquoi ? Lui seul le sait." . "<br />");
    }
}

//-----[ENTRY POINT]-----\\
$zoo1 = new Zoo("Zoo de Fistiland"); {
    $enclosure1 = new Enclosure("Enclos de ouf", $zoo1); {
        $parrot1 = new Parrot("Rio", "Mâle", 5, $enclosure1);
        $parrot2 = new Parrot("Aladdin", "Mâle", 12, $enclosure1);
        $carp1 = new Carp("Captain Fishy", "Transsexuel(le)", 7, $enclosure1);
        $carp2 = new Carp("Moby", "Femelle", 2, $enclosure1);
        $panda1 = new Panda("Bai Yun", "Mâle", 1, $enclosure1);
        $panda2 = new Panda("Bao Bao", "Femelle", 9, $enclosure1);
        $snake1 = new Snake("Asmodeus", "Mâle", 6, $enclosure1);
        $snake2 = new Snake("Jafaar", "Mâle", 3, $enclosure1);
    }
    $enclosure2 = new Enclosure("Enclos bof bof", $zoo1); {
        $carp3 = new Carp("Killmi", "Mâle", 4, $enclosure2);
    }
    $enclosure3 = new Enclosure("Enclos vraiment banal", $zoo1); {
        $panda3 = new Panda("Po", "Mâle", 6, $enclosure3);
        $parrot3 = new Parrot("Leon", "Mâle", 12, $enclosure3);
        $parrot4 = new Parrot("Hannibal", "Mâle", 20, $enclosure3);
        $parrot5 = new Parrot("Poupi", "Mâle", 11, $enclosure3);
        $carp3 = new Carp("Bubble", "Transsexuel(le)", 3, $enclosure3);
    }
    $enclosure4 = new Enclosure("Enclos concernant les poissons", $zoo1); {
        $carp4 = new Carp("Jack", "Mâle", 2, $enclosure4);
        $carp5 = new Carp("Bubba", "Mâle", 1, $enclosure4);
        $carp6 = new Carp("Finley", "Mâle", 3, $enclosure4);
        $carp7 = new Carp("Blue", "Femelle", 5, $enclosure4);
        $carp8 = new Carp("Shadow", "Femelle", 5, $enclosure4);
        $carp9 = new Carp("Jewel", "Femelle", 4, $enclosure4);
        $carp10 = new Carp("Ariel", "Femelle", 6, $enclosure4);
        $clownFish1 = new ClownFish("Nemo", "Mâle", 1, $enclosure4);
        $clownFish1 = new ClownFish("Marin", "Mâle", 7, $enclosure4);
    }
}

//-----[OUTPUT]-----\\
echo ("<h1 id='title' class='text-center mt-4 mb-4'>Zoo</h1>");
echo ("<div id='zooName' class='h5 shadow-sm rounded mt-3 mb-3 pt-3 pb-4 text-center' id='' style='margin: auto; width: 90%; height: 90%; background-color: rgba(0,0,0,.07);'><b>" . $zoo1->getName() . "</b>");
foreach ($zoo1->getEnclosures() as $key1 => $enclosure) {
    echo ("<div id='enclosureName' class='shadow-sm rounded mt-3 mb-2 pt-3 pb-3 ' style='margin: auto; width: 90%; height: 90%; background-color: rgba(0,0,100,.07); text-align: center;'>" . "Enclos n°" . ($key1 + 1) . " : " . $enclosure->getName());
    echo ("<div id='enclosureContent' class='h6 d-flex flex-wrap  justify-content-center' style='margin: auto; width: 90%; height: 90%; text-align: center;'>");
    foreach ($enclosure->getAnimals() as $key2 => $animal) {
        echo ("<div class='shadow-sm rounded mt-3 mb-2 pt-2 pb-2 pl-1 pr-1' style='margin-left: 2em; width: 15%; height: 10%; background-color: rgba(100,0,0,.07); text-align: center; display: block;'>" . "Animal n°" . ($key2 + 1) . "<br />Nom : " . $animal->getName() . "<br />Âge : " . $animal->getAge() . "<br />Genre : " . $animal->getGender() . "<br />Famille : " . $animal->getFamily() . "<br />Race : " . $animal->getRace());
        echo ("</div>");
    }
    echo ("</div>");
    echo ("</div>");
}
echo ("</div>");
